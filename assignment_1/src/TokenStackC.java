public class TokenStackC implements TokenStack{

    private Token[] tokenArray;
    private int numberOfElements;
    public static final int INIT_SIZE = 10;

    public TokenStackC() {
        tokenArray = new Token[INIT_SIZE];
        numberOfElements = 0;
    }

    public TokenStackC(TokenStackC source){
        numberOfElements = source.numberOfElements;
        copyElements(tokenArray, source.tokenArray, numberOfElements);
    }

    private void copyElements(Token[] stack, Token[] stack1, int numberOfElements) {
        for (int i = 0; i < numberOfElements; i++){
            stack[i] = new TokenC((TokenC)stack1[i]);
        }
    }

    @Override
    public void push(Token token){
        if(numberOfElements == tokenArray.length - 1){
            doubleArray();
        }

        tokenArray[numberOfElements] = token;
        numberOfElements += 1;
    }

    @Override
    public Token pop(){
        if(numberOfElements == 0){
            System.exit(0);
        }

        numberOfElements -= 1;
        return tokenArray[numberOfElements];
    }

    @Override
    public Token top(){
        return tokenArray[numberOfElements - 1];
    }

    @Override
    public int size() {
        return numberOfElements;
    }

    private void doubleArray() {
        Token[] temp = new Token[2 * tokenArray.length];

        copyElements(temp, tokenArray, numberOfElements);
        tokenArray = temp;
    }
}

