class DoubleStackC implements DoubleStack{

    private Double[] doubleArray;
    private int numberOfElements;
    public static final int INIT_SIZE = 10;

    public DoubleStackC() {
        doubleArray = new Double[INIT_SIZE];
        numberOfElements = 0;
    }

    public DoubleStackC(DoubleStackC source){
        numberOfElements = source.numberOfElements;
        copyElements(doubleArray, source.doubleArray, numberOfElements);
    }

    private void copyElements(Double[] doubleArray, Double[] doubleArray1, int numberOfElements) {
        for (int i = 0; i < numberOfElements; i++){
            doubleArray[i] = doubleArray1[i];
        }
    }

    @Override
    public void push(Double d){
        if(numberOfElements == doubleArray.length - 1){
            doubleArray();
        }

        doubleArray[numberOfElements] = d;
        numberOfElements += 1;
    }

    @Override
    public Double pop(){
        if(numberOfElements == 0){
            System.exit(0);
        }

        numberOfElements -= 1;
        return doubleArray[numberOfElements];
    }

    @Override
    public Double top(){
        return doubleArray[numberOfElements - 1];
    }

    @Override
    public int size(){
        return numberOfElements;
    }

    private void doubleArray() {
        Double[] temp = new Double[2 * doubleArray.length];

        copyElements(temp, doubleArray, numberOfElements);
        doubleArray = temp;
    }
}
