public class TokenC implements Token {

    private String value;
    private int type,
            precedence;

    public TokenC(String value, int type, int precedence) {
        this.value = value;
        this.type = type;
        this.precedence = precedence;

    }

    TokenC(TokenC source){
        value = source.value;
        type = source.type;
        precedence = source.precedence;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public int getPrecedence() {
        return precedence;
    }
}