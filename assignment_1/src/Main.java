import java.io.PrintStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main implements CalculatorInterface {

    PrintStream out;

    Main() {
        out = new PrintStream(System.out);
    }

    private static final String OPERATOR_TOKENS = "+-*/^",
                                PARENTHESES_TOKENS = "()",
                                PLUS_TOKEN = "+",
                                MINUS_TOKEN = "-",
                                MULTPLY_TOKEN = "*",
                                DIVIDE_TOKEN = "/",
                                POWER_TOKEN = "^";

    private final static int    NUMBER_TYPE = 1,
                                OPERATOR_TYPE = 2,
                                PARENTHESIS_TYPE = 3,

                                FIRST_ORDER_PRECEDENCE  = 1,
                                SECOND_ORDER_PRECEDENCE = 2,
                                THIRD_ORDER_PRECENDECE  = 3,
                                NO_PRECEDENCE = -1;

    public TokenList readTokens(String input){
        Scanner scanner = new Scanner(input);
        TokenList list = new TokenListC();

        while (scanner.hasNext()) {
            String token = scanner.next();
            if (stringIsDouble(token)) {
                list.add(parseToken(token, NUMBER_TYPE, NO_PRECEDENCE));
            } else if (stringIsOperator(token)) {
                list.add(parseToken(token, OPERATOR_TYPE, operatorPrecedence(token)));
            } else if (stringIsParentheses(token)) {
                list.add(parseToken(token, PARENTHESIS_TYPE, NO_PRECEDENCE));
            } else {
                out.printf("Invalid input \"%s\" will not be processed.\n", token);
            }
        }
        return list;
    }

    private boolean stringIsDouble(String string) {
        Scanner in = new Scanner(string);
        return in.hasNextDouble();
    }

    private boolean stringIsOperator(String string) {
        return OPERATOR_TOKENS.contains(string);
    }

    private boolean stringIsParentheses(String string) {
        return PARENTHESES_TOKENS.contains(string);
    }

    private int operatorPrecedence(String string) {
        if (string.equals("+") || string.equals("-")) {
            return FIRST_ORDER_PRECEDENCE;
        } else if (string.equals("/") || string.equals("*")) {
            return SECOND_ORDER_PRECEDENCE;
        } else if (string.equals("^")) {
            return THIRD_ORDER_PRECENDECE;
        } else {
            return NO_PRECEDENCE;
        }
    }

    private Token parseToken(String token, int type, int precedence) {
        return new TokenC(token, type, precedence);
    }

    public Double rpn(TokenList tokens) {
        DoubleStack stack = new DoubleStackC();

        for (int i = 0; i < tokens.size(); i++) {
            if (tokens.get(i).getType() == 1) {
                stack.push(Double.parseDouble(tokens.get(i).getValue()));
            } else {
                performOperation(tokens.get(i), stack);
            }
        }
        return stack.pop();
    }

    private void performOperation(Token operator, DoubleStack stack) {
        double a = stack.pop();
        double b = stack.pop();

        switch (operator.getValue()) {
            case PLUS_TOKEN :
                stack.push(a + b);
                break;
            case MINUS_TOKEN :
                stack.push(b - a);
                break;
            case MULTPLY_TOKEN :
                stack.push(a * b);
                break;
            case DIVIDE_TOKEN :
                stack.push(b / a);
                break;
            case POWER_TOKEN :
                stack.push(Math.pow(b,a));
                break;
        }
    }

    public TokenList shuntingYard(TokenList tokens) {
        TokenList outputList = new TokenListC();
        TokenStack operatorStack = new TokenStackC();

        for (int i = 0 ; i < tokens.size() ; i++) {
            if (tokens.get(i).getType() == 1) {
                outputList.add(tokens.get(i));
            } else if (tokens.get(i).getType() == 2) {
                while (operatorStack.size() != 0 && operatorStack.top().getPrecedence() >= tokens.get(i).getPrecedence()) {
                    outputList.add(operatorStack.pop());
                }
                operatorStack.push(tokens.get(i));
            }

            if (tokens.get(i).getValue().equals("(")) {
                operatorStack.push(tokens.get(i));
            }

            if (tokens.get(i).getValue().equals(")")) {
                while (!operatorStack.top().getValue().equals("(")) {
                    outputList.add(operatorStack.pop());
                }
                operatorStack.pop();
            }
        }
        while (operatorStack.size() != 0) {
            outputList.add(operatorStack.pop());
        }

        return outputList;
    }

    private void start(){
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String input = scanner.nextLine();
            System.out.format("%.6f\n", rpn(shuntingYard(readTokens(input))));
        }
        scanner.close();
    }

    public static void main(String[] argv){
        new Main().start();
    }
}