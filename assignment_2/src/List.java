public class List<E extends Comparable> implements ListInterface<E>{

    private class Node {

        E data;
        Node prior,
                next;

        public Node(E d) {
            this(d, null, null);
        }

        public Node(E data, Node prior, Node next) {
            this.data = data == null ? null : data;
            this.prior = prior;
            this.next = next;
        }

    }

    Node    current,
            first,
            last;

    private int numberOfNodes;


    List() {
        init();
    }


    @Override
    public boolean isEmpty() {
        return numberOfNodes == 0;
    }

    @Override
    public ListInterface<E> init() {
        current = null;
        first = null;
        last = null;
        numberOfNodes = 0;

        return this;
    }

    @Override
    public int size() {
        return numberOfNodes;
    }

    @Override
    public ListInterface<E> insert(E d) {
        Node temp = new Node(d);

        if (isEmpty()) {
            current = temp;
            first = current;
            last = current;
        }
        else if (first.data.compareTo(d) >= 0) {
            temp.next = first;
            first.prior = temp;
            first = first.prior;
            current = first;
        }
        else if (last.data.compareTo(d) <= 0) {
            temp.prior = last;
            last.next = temp;
            last = last.next;
            current = last;
        }
        else {
            if (find(d)) { }
            temp.next = current.next;
            temp.prior = current;
            current.next.prior = temp;
            current.next = current.next.prior;

        }
        numberOfNodes += 1;

        return this;
    }

    @Override
    public E retrieve() {
        return current.data;
    }

    @Override
    public ListInterface<E> remove() {
        if (!isEmpty()) {
             if (numberOfNodes == 1) {
                return init();
            }
            else if (current.prior == null) {
                first = current.next;
                current = first;
                current.prior = null;

            }
            else if (current.next == null) {
                last = current.prior;
                current.prior.next = null;
                current = last;

            }
            else {
                current = current.next;
                current.prior.prior.next = current;
                current.prior = current.prior.prior;
            }
            numberOfNodes -= 1;
        }
        return this;
    }

    @Override
    public boolean find(E d) {
        if (goToFirst()) {
            while (current.data.compareTo(d) != 0) {
                if (current == last) {
                    return false;
                }
                if (current.data.compareTo(d) < 0 && (current.next.data.compareTo(d) < 0 || current.next.data.compareTo(d) == 0)) {
                    current = current.next;
                } else {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }
    }

    @Override
    public boolean goToFirst() {
        if (isEmpty()) {
            return false;
        }
        if (current == first) {
            return true;
        }
        current = first;
        return true;
    }

    @Override
    public boolean goToLast() {
        if (isEmpty()) {
            return false;
        }
        if (current == last) {
            return true;
        }
        current = last;
        return true;
    }

    @Override
    public boolean goToNext() {
        if (isEmpty() || current.next == null) {
            return false;
        }
        current = current.next;
        return true;
    }

    @Override
    public boolean goToPrevious() {
        if (isEmpty() || current.prior == null) {
            return false;
        }
        current = current.prior;
        return true;
    }

    @Override
    public ListInterface<E> copy() {
        List<E> copy = new List<E>();

        if (goToFirst()) {
            do {
                copy.insert(current.data);
            }   while (goToNext());
        } else {
            return new List<E>();
        }

        return copy;
    }
}
