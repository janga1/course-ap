import java.io.PrintStream;
import java.util.Scanner;
import java.util.HashMap;
import java.math.BigInteger;
import java.util.regex.Pattern;

public class Main {

    HashMap<Identifier, Set<BigInteger>> hmap;
    PrintStream out;

    Main() {
        hmap = new HashMap<Identifier, Set<BigInteger>>();
        out = new PrintStream(System.out);
    }

    private void statement(Scanner in) throws APException{
        in.useDelimiter("");
        readspace(in);

        if (nextCharIsLetter(in)) {
            assignment(in);
        } else if (nextCharIs(in,'?')) {
            printStatement(in);
        } else if (nextCharIs(in,'/')) {
            //Do nothing if comment
            return;
        } else {
            throw new APException("Input error: Incorrect statement, {assignment | print_statement | comment}.\n");
        }
    }

    private void assignment(Scanner in) throws APException{
        readspace(in);
        Identifier identifier = identifier(in);
        readspace(in);

        if (nextCharIs(in,'=')) {
            nextChar(in);

            Set<BigInteger> set = expression(in);

            readspace(in);
            readEoln(in);

            hmap.put(identifier, set);
        } else {
            throw new APException("Input error: Incorrect assignment notation.\n");
        }
    }

    private void printStatement(Scanner in) throws APException {
        Set<BigInteger> temp = new Set<BigInteger>();
        
        readCharacter(in,'?');
        readspace(in);
        
        Set<BigInteger> set = expression(in);
        temp = set.copy();
        
        readEoln(in);
        
        printSet(temp);
    }


    private Identifier identifier(Scanner in) throws APException {
        StringBuffer identifier = new StringBuffer();
        if (nextCharIsLetter(in)) {
            identifier.append(in.next());
            while (nextCharIsAlphaNum(in)) {
                identifier.append(in.next());
            }

            return new Identifier(identifier.toString());
        } else {
            throw new APException("Input error: Identifier did not start with a letter.\n");
        }
    }
    //    private Identifier identifier(Scanner in) throws APException {
//        StringBuffer identifier = new StringBuffer();
//        if (nextCharIsLetter(in)) {
//            identifier.append(in.next());
//            while (nextCharIsAlphaNum(in)) {
//                identifier.append(in.next());
//            }
//
//            return new Identifier(identifier);
//        } else {
//            throw new APException("Input error: Identifier did not start with a letter.\n");
//        }
//    }

    private Set<BigInteger> expression(Scanner in) throws APException {
        Set<BigInteger> set = term(in);

        while (in.hasNext()) {
            readspace(in);

            if (in.hasNext()) {
                if (nextCharIs(in,')')) {
                    return set;
                }
                //Checking Operator
                set = additiveOperator(in, nextChar(in), set);
            }
        }
        return set;
    }

    private Set<BigInteger> term(Scanner in) throws APException {
        Set<BigInteger> set = factor(in);
        readspace(in);

        if (nextCharIs(in,'*')) {
            return multiplicativeOperator(in, set);
        }

        return set;
    }

    private Set<BigInteger> factor(Scanner in) throws APException {
        readspace(in);
        Set<BigInteger> temp = new Set<BigInteger>();

        if (nextCharIsLetter(in)) {
            Identifier identifier = identifier(in);
            if (hmap.containsKey(identifier)) {
                temp = hmap.get(identifier);
            } else {
                throw new APException("Key/identifier : \"" + identifier.toString() + "\" not in storage\n");
            }
        } else if (nextCharIs(in,'(')) {
            temp = complexFactor(in);
        } else if (nextCharIs(in,'{')) {
            temp = readSet(in);
        } else {
            if (!in.hasNext()) {
                throw new APException("Line ended unexpectedly.\n");
            } else {
                throw new APException("Input error: \n" + in.nextLine());
            }
        }
        readspace(in); //Redundant
        return temp;
    }

    private Set<BigInteger> complexFactor(Scanner in) throws APException {
        Set<BigInteger> set;

        readCharacter(in,'(');

        set = expression(in);

        readCharacter(in,')');

        return set;
    }

    private Set<BigInteger> readSet(Scanner in) throws  APException {
        Set<BigInteger> set = new Set<BigInteger>();
        readspace(in);

        readCharacter(in,'{');

        while (in.hasNext()) {
            readspace(in);

            if (nextCharIs(in, '}')) {
                break;
            } else if (nextCharIsDigit(in)) {
                set = rowNaturalNumbers(in);
            } else {
                throw new APException("Input error: Set is not comprised of only integers\n");
            }
        }
        readCharacter(in,'}');
        return set;
    }

    private Set<BigInteger> rowNaturalNumbers(Scanner in) throws APException {
        Set<BigInteger> set = new Set<BigInteger>();
        set.add(naturalNumber(in));

        while (nextCharIs(in,',')) {
            readCharacter(in,',');
            set.add(naturalNumber(in));
        }
        return set;
    }

    private Set<BigInteger> additiveOperator(Scanner in, char c, Set<BigInteger> set) throws APException {
        Set<BigInteger> temp = set.copy();
        switch (c) {
            case    '+':    temp = temp.union(term(in)).copy();                  break;
            case    '-':    temp = temp.complement(term(in)).copy();             break;
            case    '|':    temp = temp.symmetricDifference(term(in)).copy();    break;
            default: readCharacter(in, c);
        }
        return temp;
    }

    private Set<BigInteger> multiplicativeOperator(Scanner in, Set<BigInteger> set) throws APException {
        Set<BigInteger> temp = set.copy();
        readCharacter(in,'*');
        return temp.intersection(term(in)).copy();
    }

    private BigInteger naturalNumber(Scanner in) throws APException{
        BigInteger bi;
        readspace(in);

        if (nextCharIsNotZero(in)) {
            bi = positiveNumber(in);
        } else if (nextCharIsZero(in)) {
            bi = new BigInteger(zero());
        } else {
            throw new APException("Input Error: Set does not contain only natural numbers\n");
        }
        return bi;
    }

    private BigInteger positiveNumber(Scanner in) throws APException {
        StringBuffer sb = new StringBuffer();
        sb.append(notZero(in));
        while (in.hasNext()) {
            if (nextCharIs(in,',')) {
                break;
            } else if (nextCharIs(in,'}')) {
                break;
            } else {
                sb.append(number(in));
            }
        }
        return new BigInteger(sb.toString());
    }

    private String number(Scanner in) throws APException {
        if (nextCharIsZero(in)) {
            return zero();
        } else if (nextCharIsNotZero(in)) {
            return notZero(in);
        } else {
            throw new APException("Input Error: Set contains non-number\n");
        }
    }

    private String zero() {
        String zero = "0";
        return zero;
    }

    private String notZero(Scanner in) throws APException {
        if (!nextCharIsDigit(in)){
            throw new APException("Input Error: Set contains a negative number\n");
        }
        return in.next();
    }

    char nextChar(Scanner in) {
        return in.next().charAt(0);
    }

    boolean nextCharIs(Scanner in, char c) {
        return in.hasNext(Pattern.quote(c+""));
    }


    boolean nextCharIsLetter(Scanner in) {
        return in.hasNext("[a-zA-Z]");
    }

    boolean nextCharIsDigit(Scanner in) {
        return in.hasNext("[0-9]");
    }

    boolean nextCharIsNotZero(Scanner in) {
        return in.hasNext("[1-9]");
    }

    boolean nextCharIsZero(Scanner in) {
        return in.hasNext("[0]");
    }

    private boolean nextCharIsAlphaNum(Scanner in) {
        return in.hasNext("[a-zA-Z0-9]");
    }

    private void readspace(Scanner in) {
        while (nextCharIs(in,' ')) {
            nextChar(in);
        }
    }

    void readCharacter(Scanner in, char c) throws APException {
        if (!nextCharIs(in, c)) {
            throw new APException("Input error: " + in.nextLine());
        }
        nextChar(in);
    }

    void readEoln (Scanner in) throws APException {
        if (in.hasNext()) {
            throw new APException("Input error: " + in.nextLine());
        }
    }

    void printSet(Set<BigInteger> set) throws APException {
        while (set.size() > 0) {
            out.print(set.getElement());
            out.print(" ");
        }
        out.print("\n");
    }

    private void start() {
        Scanner in = new Scanner(System.in);

        while (in.hasNextLine()) {
            try {
                String line = in.nextLine();
                statement(new Scanner(line));
            } catch (APException e) {
                out.print(e);
            }
        }
    }

    public static void main(String[] argv) throws APException{
        new Main().start();
    }
}